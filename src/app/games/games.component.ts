import { Component, OnInit } from '@angular/core';
import { GamesService } from "../services/games.service";
import { FormControl } from "@angular/forms";
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {

  games: any;
  formControl!: FormControl;

  toppings: FormGroup;

  constructor(private readonly gamesService: GamesService, fb: FormBuilder) {
    this.toppings = fb.group({
      action: false,
      strategy: false,
      adventures: false
    });
  }

  async ngOnInit(): Promise<void> {
    this.formControl = new FormControl('');
    this.games = await this.gamesService.getGames().toPromise();
  };

  async search(): Promise<void> {
    this.games = await this.gamesService.search(this.formControl.value).toPromise();
  };

  async show(): Promise<void> {
    this.games = await this.gamesService.show(this.formControl.value).toPromise();
  }

}
