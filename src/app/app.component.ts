import { Component } from '@angular/core';

const items: string[] = [
  'games', 'library', 'friends', 'profile'
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'homeWorkAngular';

  submitted: boolean = false;
  onSubmit() { this.submitted = true; }

  condition: boolean=true;

  toggle(){
    this.condition=!this.condition;
  }

  public items: string[] = items;
  public activeItem: string | undefined;

  public onSelectItem(item: string): void {
    this.activeItem = item;
  }
}
